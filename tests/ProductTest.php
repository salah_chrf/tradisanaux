<?php
require("./src/models/Product.php");

use PHPUnit\Framework\Constraint\Count;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase{
    public function testGetAll(){
        $products=Product::getAll();
        $this->assertCount(count($products),$products);
    }
    public function testPagination (){
        $products=Product::pagination(null);
        $this->assertCount(2,$products);
    }
}