<?php

use PHPUnit\Framework\TestCase;
require_once "./src/models/User.php";

class UserTest extends TestCase{
    public function testLogin(){
        $items=User::login(['pseudo'=>'admin']);
        $this->assertEquals('admin',$items->pseudo);
    }
    public function testRegister(){
        $data=['nomComplet'=>'testTest','pseudo'=>'test123','email'=>'test@email.com','motpass'=>'test123'];
        $result=User::createUser($data);
        $this->assertEquals("ok",$result);
    }
}