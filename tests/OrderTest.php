<?php

use PHPUnit\Framework\TestCase;
require_once "./src/models/Order.php";

class OrderTest extends TestCase{
    public function testAllOrders(){
        $res=Order::getAllOrders();
        $num=count($res);
        $this->assertCount($num,$res);
    }
    public function testPrixTtl(){
        $res=Order::getTotalOrdersPrice();
        $this->assertIsNumeric($res['total']);
    }
}