<?php

use PHPUnit\Framework\TestCase;
require_once "./src/models/Category.php";

class CategoryTest extends TestCase{
    public function testAddCatgeory(){
        $res=Category::AjouterCtg("test_ctg");
        $this->assertEquals("ok",$res);
    }
    public function testDeleteCatgeory(){
        $res=Category::DeleteCtg("last_insert_id()");
        $this->assertEquals("ok",$res);
    }
}