<?php
class DB{
    private static $db;
    public static function connect(){
        $dsn = 'mysql:host=localhost;dbname=db_test_tradisanaux';
        $user='root';
        $pass='root1234';
        if(!isset(DB::$db)){
            DB::$db=new PDO($dsn,$user,$pass);
        }
        DB::$db->exec('set names utf8');
        DB::$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
        return DB::$db;
    }
    
    public static function executerRequete($sql,$params=[]){
        $db=self::connect();
        $stm=$db->prepare($sql);
        $stm->execute($params);
        return $stm;
    }
}

