<?php
class UserController
{
    public function auth()
    {
        if (isset($_POST['send'])) {
            $data['pseudo'] = $_POST['pseudo'];
            $result = User::login($data);
            if ($result->pseudo === $_POST['pseudo'] && password_verify($_POST['motpass'], $result->motpass)) {
                $_SESSION['logged'] = true;
                $_SESSION['pseudo'] = $result->pseudo;
                $_SESSION['nomComplet'] = $result->nomComplet;
                $_SESSION['idUser'] = $result->idUser;
                $_SESSION['admin'] = $result->admin;
                if($_SESSION['admin']==true){
                    $_SESSION['adminRole']=true;
                }
                Redirect::to("home");
            } else {
                Session::setCk('error', 'pseudo ou mot de pass est incorrect');
                Redirect::to('login');
            }
        }
    }

    public function register()
    {
        $data['pseudo'] = $_POST['pseudo'];
        $result = User::login($data);
        if ($result->pseudo == $_POST['pseudo']) {
            Session::setCk('error', 'Modifier le pseudo');
            Redirect::to('register');
        } else {
            $options = [
                'cost' => 12
            ];
            $password = password_hash($_POST['motpass'],PASSWORD_DEFAULT, $options);
            $dataUser = [
                'nomComplet' => $_POST['nomComplet'],
                'pseudo' => $_POST['pseudo'],
                'email' => $_POST['email'],
                'motpass' => $password
            ];
            $res=User::createUser($dataUser);
            if($res=='ok'){
                Session::setCk('success','Inscription réussie <b>Connectez vous<b>');
                Redirect::to('login');
            }else{
                Session::setCk('error','Inscription incomplet');
                Redirect::to('register');
            }
        }
    }
    public function logOut(){
        session_destroy();
        Redirect::to('home');
    }
}
