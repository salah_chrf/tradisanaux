<?php
class LigneOrderController{
    public static function addLignes($data){
        LigneOrder::createLigne($data);
        if(isset($_SESSION['clearCart']) && $_SESSION['clearCart']===true){
            foreach($_SESSION as $name=>$product){
                if(substr($name,0,9)=='products_'){
                    unset($_SESSION[$name]);
                    unset($_SESSION['count']);
                    unset($_SESSION['totaux']);
                }
            }
            unset($_SESSION['clearCart']);
            Redirect::to('home');
            Session::setCk('success','Commande effectuée');
        }
    }

    public static function getOrderDetails($id){
        if(isset($id)){
            return LigneOrder::getOrderDetails($id);
        }
    }
}