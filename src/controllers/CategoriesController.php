<?php
class CategoriesController{
    public function getAllCategories(){
        $categories=Category::getAll();
        return $categories;
    }
    public function removeCtg(){
        if(isset($_POST["deleteCtg_id"])){
            $res=Category::DeleteCtg($_POST["deleteCtg_id"]);
            if($res=="ok"){
                Session::setCk("success","catégorie bien supprimer");
            }
            elseif($res=="error"){
                Session::setCk('info',"des produits disponible sont attribuer a cette catégorie !");
            }
            Redirect::to("Ctgs");
        }
    }

    public function addCtg(){
        if(isset($_POST['Ctg_title'])){
            $res=Category::AjouterCtg($_POST['Ctg_title']);
            Session::setCk("success","catégorie bien ajouté");
            Redirect::to("Ctgs");
        }
    }
}