<?php
if(isset($_SESSION['admin']) && $_SESSION['admin'] == true){
    $data = new OrderController();
    $orders = $data->getAllOrders();
  }else{
      Redirect::to("home");
  }
?>
<!-- <?= print_r($orders)?> -->
<div class="container">
  <div class="row my-5">
    <div class="col-md-10 mx-auto">
        <div class="card bg-light p-3">
            <table class="table table-hover table-inverse">
                <h3 class="font-weight-bold">Commandes</h3>
                <thead>
                    <tr>
                        <th>Nom & Prénom</th>
                        <th>Total</th>
                        <th>Effectuée le</th>
                        <th>détaill Commande</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($orders as $order) :?>
                    <tr>
                        <td><?= $order["nomClient"];?></td>
                        <td><?= $order["total"];?> DH</td>
                        <td><?= $order["dateAchat"];?></td>
                        <td class="align-middle">
                                    <form method="POST" action="<?= BASE_URL ?>orderDetails">
                                        <input type="hidden" value="<?= $order['idCmd'] ?>" name="idCmd" />
                                        <button class="btn btn-sm btn-info" type="submit">Show details</button>
                                    </form>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
  </div>
</div>