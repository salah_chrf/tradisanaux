<?php
  if(isset($_SESSION['admin']) && $_SESSION['admin']==true){
    $data = new CategoriesController();
    $ctgs = $data->getAllCategories();
  }else{
      Redirect::to("home");
  }
?>
<div class="container">
  <div class="row my-5">
    <div class="col-md-6 mx-auto">
        <div class="form-group">
            <a href="<?php echo BASE_URL?>addCtg" class="btn btn-primary btn-sm">
                Ajouter
            </a>
        </div>
        <div class="card bg-light p-3">
            <table class="table table-hover table-inverse">
                <h3 class="font-weight-bold">Catégories</h3>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Titre</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($ctgs as $ctg) :?>
                        <!-- <?= print_r($ctg);?> -->
                    <tr>
                        <td><?=$ctg["idCtg"];?></td>
                        <td><?=$ctg["titleCtg"];?></td>
                        <td class="">
                        <form action="<?php echo BASE_URL?>deleteCtg" method="post">
                            <input type="hidden" name="deleteCtg_id" value="<?=$ctg['idCtg'];?>">
                            <button type="submit" class="btn btn-danger btn-sm">
                                Supprimer
                            </button>
                        </form>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
  </div>
</div>
                                                        
                                                    