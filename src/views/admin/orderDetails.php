<?php
if(isset($_SESSION["admin"]) && $_SESSION["admin"] == true){    
    $obj= new LigneOrder();
    $details=$obj->getOrderDetails($_POST["idCmd"]);
}else{
    Redirect::to("home");
}
?>

<table class="table table-bordered text-center mb-0 m-3">
                <thead class=" text-dark">
                    <tr>
                        <th>Produit</th>
                        <th>Prix</th>
                        <th>Qantité</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody class="align-middle">
                    <?php foreach ($details as $product) : ?>
                            <tr>
                                <td class="align-middle">
                                    <img src="./public/uploads/<?=$product['imagePrd']?>" alt="imgPrd"
                                    style="width: 50px;">
                                    <?= $product['titlePrd'] ?>
                                </td>
                                <td class="align-middle"><?= $product['prixPrd'] ?> DH</td>
                                <td class="align-middle"><?= $product['qte'] ?></td>
                                <td class="align-middle"><?= $product['prixPrd']*$product['qte'] ?> DH</td>
                            </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
