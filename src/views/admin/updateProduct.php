<?php
    if(isset($_SESSION["admin"]) && $_SESSION["admin"] == true){
        $categories = new CategoriesController();
        $categories = $categories->getAllCategories();
        $productToUpdate = new ProductsController();
        $productToUpdate = $productToUpdate->getProductById($_POST['product_id']);
        if(isset($_POST["submit"])){
            $product = new ProductsController();
            $product->updateProduct();
        }
    }else{
        Redirect::to("home");
    }
?>
<div class="container">
    <div class="row my-4">
        <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        Modifier un produit
                    </h3>
                </div>
                <div class="card-body">
                    <form method="post" class="mr-1" enctype="multipart/form-data">
                        <div class="form-group">
                            <input type="text"
                            class="form-control"
                            name="product_title"
                            required autocomplete="off"
                            placeholder="Titre"
                            value="<?php echo $productToUpdate->titlePrd;?>">
                        </div>
                        <div class="form-group">
                            <textarea row="5" cols="20" autocomplete="off" class="form-control"
                            name="product_description"
                            placeholder="Description" id=""><?php echo $productToUpdate->descriptionPrd;?>
                        </textarea>
                        </div>
                        <div class="form-group">
                            <textarea row="5" cols="20" autocomplete="off" class="form-control" name="short_desc"
                            placeholder="Description Courte" id=""><?php echo $productToUpdate->miniDscPrd;?></textarea>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="product_category_id" id="">
                                <?php foreach($categories as $category) :?>
                                    <option
                                        <?php echo $productToUpdate->ctgId===$category["idCtg"]?"selected":"";?>
                                        value="<?php echo $category["idCtg"]?>">
                                        <?php echo $category["titleCtg"]?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="number" autocomplete="off"
                            class="form-control" name="product_price"
                            placeholder="Prix"
                            value="<?php echo $productToUpdate->prixPrd;?>">
                        </div>
                        <div class="form-group">
                            <input type="number" autocomplete="off"
                            class="form-control" name="old_price"
                            placeholder="Ancien Prix"
                            value="<?php echo $productToUpdate->oldPrixPrd;?>">
                        </div>
                        <input type="hidden"
                            name="product_id"
                            value="<?php echo $productToUpdate->idPrd;?>">
                        <input type="hidden" name="product_current_image"
                            value="<?php echo $productToUpdate->imgPrd;?>">
                        <div class="form-group">
                            <input type="number" autocomplete="off"
                            class="form-control" name="product_quantity"
                            placeholder="Quantité"  value="<?php echo $productToUpdate->qntPrd;?>">
                        </div>
                        <div class="form-group">
                            <img src="./public/uploads/<?php echo $productToUpdate->imagePrd;?>"
                            width="400" height="400" alt="" class="img-fluid rounded">
                        </div>
                         <div class="form-group">
                            <input type="file"
                            class="form-control" name="image" >
                        </div>
                        <div class="form-group">
                            <button name="submit" class="btn btn-primary">
                                Valider
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
                                                        
                                                    