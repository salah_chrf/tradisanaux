<?php
  if(isset($_SESSION['admin']) && $_SESSION['admin']==true){
    $data = new ProductsController();
    $products = $data->getAllProducts();
  }else{
      Redirect::to("home");
  }
?>
<div class="container">
  <div class="row my-5">
    <div class="col-md-11 mx-auto">
        <div class="form-group">
            <a href="<?php echo BASE_URL?>addProduct" class="btn btn-primary btn-sm">
                Ajouter
            </a>
        </div>
        <div class="card bg-light p-3">
            <table class="table table-hover table-inverse">
                <h3 class="font-weight-bold">Produits</h3>
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Libellé</th>
                        <th>Prix</th>
                        <th>Quantité</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($products as $product) :?>
                        <!-- <?= print_r($product);?> -->
                    <tr>
                        <td>
                            <img width="50" height="50"
                            src="./public/uploads/<?=$product["imagePrd"];?>"
                            alt="" class="img-fluid">
                        </td>
                        <td><?=$product["titlePrd"];?></td>
                        <td><?=$product["prixPrd"];?></td>
                        <td><?=$product["qntPrd"];?></td>
                        <td><?=substr($product["descriptionPrd"],0,100);?></td>
                        <td class="d-flex flex-row justify-content-center align-items-center">
                        <form  action="<?php echo BASE_URL?>updateProduct" method="post">
                            <input type="hidden" name="product_id" id="product_id" value="<?=$product['idPrd'];?>">
                            <button type="submit" class="btn btn-warning btn-sm mr-1">
                                Modifier
                            </button>
                        </form>
                        <form action="<?php echo BASE_URL?>deleteProduct" method="post">
                            <input type="hidden" name="deleteP_id" value="<?=$product['idPrd'];?>">
                            <button type="submit" class="btn btn-danger btn-sm">
                                Supprimer
                            </button>
                        </form>
                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
  </div>
</div>
                                                        
                                                    