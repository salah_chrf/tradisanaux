<?php
$data = new OrderController();
$datap = new ProductsController();
$orders = $data->getAllOrders();
$products = $datap->getAllProducts();
$prixTComandes = Order::getTotalOrdersPrice();
$nbrPvendus = 0;

?>

<div class="container">
  <div class="row">
    <div style="height:350px;background-color: rgb(250, 243, 229)" class="container col p-5 mx-4 mt-3">
      <div>
        <h3 class="text-warning">Commandes</h3>
      </div>
      <div class="d-flex flex-column m-4 rounded rounded-2 bg-light">
        <span class="m-1 font-4 bg-warning p-4 rounded rounde-2 text-white">Nombre total des commandes :
          <strong class="text-center">10 </strong>
        </span>
        <span class="m-1 font-4 bg-warning p-4 text-white rounded rounde-2">Prix total des commandes :
          <strong class="text-center text-white">120000</strong>
        </span>
      </div>
    </div>

    <div style="max-height:350px;background-color: rgb(250, 243, 229)" class="container col p-5  mx-4  mt-3">
      <div>
        <h3 class="text-warning">Produits</h3>
      </div>
      <div class="d-flex flex-column m-4 rounded rounded-2 bg-light">
        <span class="m-1 font-4 bg-warning p-4 rounded rounde-2 text-white">Nombre total des Produits :
          <strong class="text-center text-white"><?php echo count($products) ?></strong>
        </span>
        <span class="m-1 font-4 bg-warning p-4 rounded rounde-2 text-white">Nombre total des Ventes:
          <strong class="text-center text-white"><?= $nbrPvendus ?></strong>
        </span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col p-5">
      <figure>
        <figcaption>Dashboard Shart </figcaption>
        <svg style="padding: 120px;" viewBox="0 0 63.6619772368 63.6619772368">
          <circle class="pie1" cx="31.8309886184" cy="31.8309886184" r="15.9154943092" />
          <circle class="pie2" cx="31.8309886184" cy="31.8309886184" r="15.9154943092" />
          <circle class="pie3" cx="31.8309886184" cy="31.8309886184" r="15.9154943092" />
          <circle class="pie4" cx="31.8309886184" cy="31.8309886184" r="15.9154943092" />
        </svg>
      </figure>
    </div >
    <div class="col mt-5 p-5">
      <main>
        <svg width="400" height="400" class="mt-5" viewbox="-50 -50 400 400">
          <path id="stat" fill="none" stroke="url(#skyGradient)" stroke-miterlimit="10" stroke-width="20" stroke-linecap="round" d="M77.2,281.4C31.4,255.7,0.5,206.7,0.5,150.5c0-82.8,67.2-150,150-150s150,67.2,150,150
c0,59-34.1,110-83.6,134.5" />
          <defs>
            <linearGradient id="skyGradient" gradientTransform="rotate(90)">
              <animateTransform attributeName="gradientTransform" attributeType="XML" type="rotate" values="90;0;-90" fill="freeze" dur="5s" additive="sum" />
              <stop offset="50%" stop-color="gold" stop-opacity=".5"></stop>
              <stop offset="50%" stop-color="orange"></stop>
              " stop-opacity=".9"></stop>
            </linearGradient>
          </defs>
          <text text-anchor="middle" ; x="150" y="150" fill="gold">50%</text>
        </svg>
      </main>
    </div>
  </div>