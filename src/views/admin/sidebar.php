<nav>
    <div class="sidebar-top">
      <span class="shrink-btn">
        <i class='bx bx-chevron-left'></i>
      </span>
      <img src="/public/img/logo.png" class="logo" alt="">
      <h3 class="hide">Admin Controle</h3>
    </div>


    <div class="sidebar-links">
      <ul>
        <div class="active-tab"></div>
        <li class="tooltip-element" data-tooltip="0">
          <a href="<?=BASE_URL?>dashboard" class="active" data-active="0">
            <div class="icon">
              <i class='bx bx-tachometer'></i>
              <i class='bx bxs-tachometer'></i>
            </div>
            <span class="link hide">Dashboard</span>
          </a>
        </li>
        <li class="tooltip-element" data-tooltip="1">
          <a href="<?=BASE_URL?>orders" data-active="1">
            <div class="icon">
              <i class='bx bx-folder'></i>
              <i class='bx bxs-folder'></i>
            </div>
            <span class="link hide">Commandes</span>
          </a>
        </li>
        <li class="tooltip-element" data-tooltip="2">
          <a href="<?=BASE_URL?>products" data-active="2">
            <div class="icon">
              <i class='bx bx-message-square-detail'></i>
              <i class='bx bxs-message-square-detail'></i>
            </div>
            <span class="link hide">Produits</span>
          </a>
        </li>
        <li class="tooltip-element" data-tooltip="2">
          <a href="<?=BASE_URL?>Ctgs" data-active="2">
            <div class="icon">
            <i class='bx bx-category'></i>
            <i class='bx bx-category'></i>
            </div>
            <span class="link hide">Catégories</span>
          </a>
        </li>
        <!-- <div class="tooltip">
          <span class="show">Dashboard</span>
          <span>Projects</span>
          <span>Messages</span>
          <span>Analytics</span>
        </div> -->
      </ul>

      
    </div>

    <div class="sidebar-footer">
      <a href="#" class="account tooltip-element" data-tooltip="0">
        <i class='bx bx-user'></i>
      </a>
      <div class="admin-user tooltip-element" data-tooltip="1">
        <div class="admin-profile hide">
          <img src="/public/img/face-1.png" alt="">
          <div class="admin-info">
            <h5>Admin</h5>
          </div>
        </div>
        <a href="<?=BASE_URL?>" class="log-out">
            <i class='bx bx-home'></i>
        </a>
        <!-- <a href="<?=BASE_URL?>logout" class="log-out">
            <i class='bx bx-log-out'></i>
        </a> -->
      </div>
      <!-- <div class="tooltip">
        <span>Home</span>
        <span>Logout</span>
      </div> -->
    </div>
  </nav>