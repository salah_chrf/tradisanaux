<?php
if (isset($_POST['productId'])) :
    $data = new ProductsController();
    $product = $data->getProductById($_POST['productId']);
?>

    <div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="w-100 pt-1 mb-5 text-right">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="get" class="modal-content modal-body border-0 p-0">
                <div class="input-group mb-2">
                    <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                    <button type="submit" class="input-group-text color text-light">
                        <i class="fa fa-fw fa-search text-white"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <section class="bg-light">
        <div class="container pb-5">
            <div class="row">
                <div class="col-lg-5 mt-5">
                    <div class="card mb-3 colorB">
                        <img class="card-img img-fluid" src="./public/uploads/<?= $product->imagePrd ?>" alt="Card image cap" id="product-detail">
                    </div>
                </div>
                <div class="col-lg-7 mt-5">
                    <div class="card colorB ">
                        <div class="card-body">
                            <h1 class="h2"><?= $product->titlePrd ?></h1>
                            <p class="h3 py-2"><?= $product->prixPrd ?> DH <s class="text-decoration-line-through text-danger">(<?= $product->OldPrixPrd ?> DH)</s></p>
                            <p class="py-2">
                                <i class="fa fa-star text-warning"></i>
                                <i class="fa fa-star text-warning"></i>
                                <i class="fa fa-star text-warning"></i>
                                <i class="fa fa-star text-warning"></i>
                                <i class="fa fa-star text-secondary"></i>
                                <span class="list-inline-item text-dark">Rating 4.8 | 36 Comments</span>
                            </p>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <h6>Brand:</h6>
                                </li>
                                <li class="list-inline-item">
                                    <p class="text-muted"><strong>Tradisanaux</strong></p>
                                </li>
                            </ul>

                            <h6>Description:</h6>
                            <?= $product->miniDscPrd ?>


                            <h6 class="my-3">Specification:</h6>
                            <p>
                                <?= $product->descriptionPrd ?>
                            </p>

                            <form method="POST" action="<?= BASE_URL ?>checkout">
                                <div class="row">
                                    <div class="col-auto">
                                        <ul class=" list-inline pb-3">
                                            <li class="list-inline-item text-right ">
                                                Quantity
                                                <input type="hidden" name="qnt" id="product-quanity" value="1">
                                                <input type="hidden" name="productTitle" id="prdTitle" value="<?= $product->titlePrd ?>" />
                                                <input type="hidden" name="productId" id="prdId" value="<?= $product->idPrd ?>" />
                                            </li>
                                            <li class="list-inline-item"><span class="btn color" id="btn-minus">-</span>
                                            </li>
                                            <li class="list-inline-item"><span class="badge bg-secondary" id="var-value">1</span></li>
                                            <li class="list-inline-item"><span class="btn color" id="btn-plus">+</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row pb-3">
                                    <div class="col d-grid">
                                        <button type="submit" class="btn color btn-lg btn-outline" name="submit" value="addtocard">Add To Cart</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>