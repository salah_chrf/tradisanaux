<div class="container-fluid pt-5">
    <div class="row px-xl-5">
        <div class="col-lg-8 table-responsive mb-5 ">
            <table class="table table-warning table-hover text-center mb-0">
                <thead class="text-dark">
                    <tr>
                        <th>Produit</th>
                        <th>Prix</th>
                        <th>Qantité</th>
                        <th>Total</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody class="align-middle">
                    <?php foreach ($_SESSION as $name => $product) : ?>
                        <?php if (substr($name, 0, 9) == 'products_') : ?>
                            <tr>
                                <td class="align-middle">
                                    <img src="./public/uploads/<?= $product['image'] ?>" alt="imgPrd" style="width: 50px;">
                                    <?= $product['title'] ?>
                                </td>
                                <td class="align-middle"><?= $product['prix'] ?> DH</td>
                                <td class="align-middle col-3">
                                <?php echo $product['qte']; ?>
                                </td>
                                <td class="align-middle"><?= $product['total'] ?> DH</td>
                                <td class="align-middle">
                                    <form method="POST" action="<?= BASE_URL ?>cancelCart">
                                        <input type="hidden" value="<?= $product['id'] ?>" name="product_Id" />
                                        <input type="hidden" value="<?= $product['total'] ?>" name="product_Price" />
                                        <button id="supprimer" type="submit">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-4">
            <?php if (isset($_SESSION['count']) && $_SESSION['count'] > 0) : ?>
                <form method="POST" action="<?= BASE_URL ?>emptyCart">
                    <button id="vider" class="btn btn-lg btn-danger" type="submit">Vider le Panier</button>
                </form>
            <?php endif; ?>
            <form class="mb-3" action="">
                <div class="input-group">
                    <input type="text" id="codeC" class="form-control p-4" placeholder="Coupon Code">
                    <div class="input-group-append">
                        <button class="btn" id="apply">Apply Coupon</button>
                    </div>
                </div>
            </form>
            <div class="card mb-3">
                <div id="hdrpanier" class="card-header border-0">
                    <h4 class="font-weight-semi-bold m-0">Panier</h4>
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-between mb-3 pt-1">
                        <h6 class="font-weight-medium">Nombre des produits</h6>
                        <h6 class="font-weight-medium clr">
                            <?php
                            if (isset($_SESSION['totaux'])) {
                                echo $_SESSION['totaux'];
                            } else {
                                echo 0;
                            } ?>
                        </h6>
                    </div>
                    <div class="d-flex justify-content-between">
                        <h6 class="font-weight-medium">Frais de livraison</h6>
                        <h6 class="font-weight-medium clr">
                            <?php
                            if (isset($_SESSION['count']) && $_SESSION['count'] > 0) {
                                echo 50;
                            } else {
                                echo 0;
                            } ?>
                        </h6>
                    </div>
                </div>
                <div class="card-footer border-secondary bg-transparent">
                    <div class="d-flex justify-content-between mt-2">
                        <h5 class="font-weight-bold">Total</h5>
                        <h5 class="font-weight-bold clr">
                            <strong id="amount" data-amount='<?= $_SESSION['totaux'] ?>'>
                                <?php
                                if (isset($_SESSION['totaux']) && $_SESSION['totaux'] != 0) {
                                    echo $_SESSION['totaux'] + 50;
                                } else {
                                    echo 0;
                                } ?>
                            </strong>
                        </h5>
                    </div>
                    <?php if (isset($_SESSION['count']) && $_SESSION['count'] > 0 && isset($_SESSION['nomComplet'])) : ?>
                        <form method="POST" action="<?= BASE_URL ?>addOrder" id="addOrder">
                        </form>
                        <div id="paypal-button-container"></div>
                    <?php endif;?>
                    <?php if (!isset($_SESSION['nomComplet'])) : ?>
                        <div class="container d-flex row pt-2">
                            <div class="text-danger col-12 text-center h3 mb-3">Connectez-vous !</div>
                            <a href="<?= BASE_URL ?>login" class="btn btn-outline-success col-12 ms-2">Connexion</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    let amount = document.querySelector("#amount").dataset.amount;
    let finalAmount = Math.floor(amount / 10.58);
    paypal.Buttons({
        // Sets up the transaction when a payment button is clicked
        createOrder: (data, actions) => {
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: finalAmount.toString() // Can also reference a variable or function
                    }
                }]
            });
        },
        // Finalize the transaction after payer approval
        onApprove: (data, actions) => {
            return actions.order.capture().then(function(orderData) {
                // Successful capture! For dev/demo purposes:
                console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
                const transaction = orderData.purchase_units[0].payments.captures[0];
                // alert(`commande éfectuée: ${transaction.id}\n\nSee console for all available details`);
                // When ready to go live, remove the alert and show a success message within this page. For example:
                const element = document.getElementById('paypal-button-container');
                element.innerHTML = '<h3>Thank you for your payment!</h3>';
                document.querySelector("#addOrder").submit()
            });
        }
    }).render('#paypal-button-container');
</script>