    <!-- Modal -->
    <div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="w-100 pt-1 mb-5 text-right">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="get" class="modal-content modal-body border-0 p-0">
                <div class="input-group mb-2">
                    <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                    <button type="submit" class="input-group-text bg-success text-light">
                        <i class="fa fa-fw fa-search text-white"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>



    <!-- Start Banner Hero -->
    <div id="template-mo-zay-hero-carousel" class="carousel slide" data-bs-ride="carousel">
        <ol class="carousel-indicators">
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="0" class="active"></li>
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="1"></li>
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" style="margin-left: 50px;" src="./public/img/belghziwani.png" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left align-self-center">
                                <h1 class="h1 text-success"><b>LES BABOUCHES MAROCAINES</b></h1>

                                <p>
                                    Les babouches sont également un symbole de l'habillement marocain typique. Cette mode se décline aujourd'hui sous toutes les coutures
                                    et notre magazin proposent des modèles particulièrement design et contemporains !
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" style="width: 300px;margin-left: 200px;margin-bottom: 50px;" src="./public/img/fanous.png" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left">
                                <h1 class="h1">DECORATION EN BOIS</h1>

                                <p>
                                    la décoration en bois est une spécialité marocaine artisanale qui a bien su sa place dans le monde de la décoration. Dans la même veine, c'est la marqueterie,
                                    décorations en plaques de bois typiques, que vous pourrez retrouver un peu partout au Maroc
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-8 col-lg-6 order-lg-last">
                            <img class="img-fluid" style="margin-left: 200px;margin-bottom: 50px;width: 250px;" src="./public/img/sac-a-dos.png" alt="">
                        </div>
                        <div class="col-lg-6 mb-0 d-flex align-items-center">
                            <div class="text-align-left">
                                <h1 class="h1"> CUIR </h1>

                                <p>
                                    Nos produits traditionnels en cuir sont le fruit de notre engagement à proposer des accessoires de qualité qui allient style et fonctionnalité de manière inégalée. Fabriqués à partir de matériaux de qualité supérieure par nos artisans talentueux,
                                    ils sont solides, durables et inspirés par les riches traditions culturelles et historiques de notre région. Que vous cherchiez un sac à dos, une paire de babouches ou tout autre produit en cuir, nous avons ce qu'il vous faut.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev text-decoration-none w-auto ps-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="prev">
            <i class="fas fa-chevron-left"></i>
        </a>
        <a class="carousel-control-next text-decoration-none w-auto pe-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="next">
            <i class="fas fa-chevron-right"></i>
        </a>
    </div>
    <!-- End Banner Hero -->


    <!-- Start Categories of The Month -->
    <section class="container py-5">
        <div class="row text-center pt-3">
            <div class="col-lg-6 m-auto">
                <h1 class="h1">Catégories du mois</h1>
                <p>
                    "Notre sélection de catégories du mois met en avant des produits de qualité, choisis pour leur originalité et leur lien avec les traditions culinaires de notre région.
                    Nous sommes convaincus que ces produits offriront une expérience gustative unique et exceptionnelle à nos clients."
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="#"><img src="./public/img/zouake-2.jpg" style="height: 270px;" class="rounded-circle img-fluid border"></a>
                <h5 class="text-center mt-3 mb-3">Art de bois</h5>
                <p class="text-center"><a class="btn btn-success">Voir plus</a></p>
            </div>
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="#"><img src="./public/img/babouches.jpg" style="height: 270px;" class="rounded-circle img-fluid border"></a>
                <h2 class="h5 text-center mt-3 mb-3">Babouches et sandales</h2>
                <p class="text-center"><a class="btn btn-success">Voir plus</a></p>
            </div>
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="#"><img src="./public/img/tapis.jpg" style="height: 270px;" class="rounded-circle img-fluid border"></a>
                <h2 class="h5 text-center mt-3 mb-3">Tapis</h2>
                <p class="text-center"><a class="btn btn-success">Voir plus</a></p>
            </div>
        </div>
    </section>
    <!-- End Categories of The Month -->


    <!-- Meilleures ventes -->
    <section class="bg-light">
        <div class="container py-5">
            <div class="row text-center py-3">
                <div class="col-lg-6 m-auto">
                    <h1 class="h1">Meilleures ventes</h1>
                    <p>
                        "Des goûts exceptionnels aux traditions culinaires de notre région en passant par leur popularité auprès de nos clients,
                        nos meilleures ventes sont le résultat de notre engagement à proposer les meilleurs produits de qualité supérieure."
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="#">
                            <img src="./public/img/sandale chic marakech.jpg" class="card-img-top" alt="...">
                        </a>
                        <div class="card-body">
                            <ul class="list-unstyled d-flex justify-content-between">
                                <li>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-muted fa fa-star"></i>
                                    <i class="text-muted fa fa-star"></i>
                                </li>
                                <li class="text-muted text-right">180.00 MAD</li>
                            </ul>
                            <a href="shop-single.html" class="h2 text-decoration-none text-dark">sandale chic marakech</a>
                            <p class="card-text mt-2">
                                -Une belle Sandale femme 100 % artisanale fabriquée avec de véritable cuir et de matière bio de haute qualité ,
                                parfaite pour les beaux jours elle est très confortable et élégante.
                                Souple et légère elle est entièrement faite a là main
                                Toutes les étapes de fabrication sont réalisées à la main </p>
                            <p class="text-muted">Reviews (24)</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="#">
                            <img src="./public/img/ceinture homme 28.jpg" class="card-img-top" alt="...">
                        </a>
                        <div class="card-body">
                            <ul class="list-unstyled d-flex justify-content-between">
                                <li>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-muted fa fa-star"></i>
                                    <i class="text-muted fa fa-star"></i>
                                </li>
                                <li class="text-muted text-right">150.00 MAD</li>
                            </ul>
                            <a href="shop-single.html" class="h2 text-decoration-none  text-dark">Ceinture en cuir </a>
                            <p class="card-text mt-2">
                                -ceintures pour homme 100% cuir, conçues à partir de matériaux de très haute qualité pour une usure de longue durée.
                                Ces ceintures sont très élegantes et fines,et ce spécifie par ses motifs traditionaux . </p>
                            <p class="text-muted">Reviews (48)</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-4">
                    <div class="card h-100">
                        <a href="#">
                            <img src="./public/img/pach work 1.jpg" class="card-img-top" alt="...">
                        </a>
                        <div class="card-body">
                            <ul class="list-unstyled d-flex justify-content-between">
                                <li>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-warning fa fa-star"></i>
                                    <i class="text-warning fa fa-star"></i>
                                </li>
                                <li class="text-muted text-right">500.00 MAD </li>
                            </ul>
                            <a href="shop-single.html" class="h2 text-decoration-none text-dark">pach work </a>
                            <p class="card-text mt-2">
                                -Utilisez tous les chiffons pour coudre un chemin de table original !
                                Il ajoutera une ambiance chic à votre table et la protégera des plats surchauffés.
                                Il peut remplacer les nappes de manière très avantageuse.
                                Sa fabrication est basée sur le recyclage des vieux vêtements en paillettes, connu également sous le nom du Patchwork. </p>
                            <p class="text-muted">Reviews (74)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Fin Meilleures vente -->