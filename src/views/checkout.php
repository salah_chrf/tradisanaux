<?php
if(isset($_POST['productId'])){
    $id=$_POST['productId'];
    $data=new ProductsController();
    $product=$data->getProductById($id);
    if($_SESSION["products_".$id]["title"]==$_POST['productTitle']
    ){
        Session::setCk('error','produit déja ajouter au panier');
        Redirect::to('cart');
    }else{
        if($product->qntPrd<$_POST['qnt']){
            Session::setCk('info','la qantité disponible de produit : '.$product->qntPrd);
            Redirect::to('cart');
        }else{
            $_SESSION['products_'.$product->idPrd]=array(
                'id'=>$product->idPrd,
                'image'=>$product->imagePrd,
                'title'=>$product->titlePrd,
                'prix'=>$product->prixPrd,
                'qntTotalPrd'=>$product->qntPrd,
                'qte'=>intval($_POST['qnt'])==0?1:intval($_POST['qnt']),
                'total'=>(intval($_POST['qnt'])==0?1:intval($_POST['qnt']))*$product->prixPrd
            );
            $_SESSION['totaux']+=$_SESSION['products_'.$product->idPrd]['total'];
            $_SESSION['count']+=1;
            Session::setCk('success','la produit a été bien ajouté');
            Redirect::to('cart');
        }
    }
}
