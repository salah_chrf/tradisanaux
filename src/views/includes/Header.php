<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://www.paypal.com/sdk/js?client-id=ARs2OvahvtyJNMBCPRxsG-eeKKkK-IMd2dP-s1om6Y_16QnGgGmz9vg1hyvMKnQhYmtLXneYmOqYe932&currency=USD"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Varela+Round">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="./public/css/navbar.css">

  <link rel="apple-touch-icon" href="public/img/apple-icon.png">
  <link rel="shortcut icon" type="image/x-icon" href="public/img/favicon.ico">

  <link rel="stylesheet" href="public/css/bootstrap.min.css">
  <link rel="stylesheet" href="public/css/templatemo.css">
  <link rel="stylesheet" href="public/css/custom.css">
  <link rel="stylesheet" href="public/css/cart.css">


  <!-- 404 links -->
  <!-- Google font -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,700" rel="stylesheet">

  <!-- Custom stlylesheet -->
  <link type="text/css" rel="stylesheet" href="/assets/css/css404.css" />


  <!-- Load fonts style after rendering the layout styles -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
  <link rel="stylesheet" href="public/css/fontawesome.min.css">
  <title>Tradisanaux</title>
  <!-- <link rel="stylesheet" href="public/css/login.css"> -->
  <script>
    // Prevent dropdown menu from closing when click inside the form
    $(document).on("click", ".action-buttons .dropdown-menu", function(e) {
      e.stopPropagation();
    });
  </script>
  <!-- tostify -->
  <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
</head>

<body>
  <?php include('navbar.php') ?>
  <div class="col-6 mx-auto m-3">
    <?php include('alerts.php'); ?>
  </div>