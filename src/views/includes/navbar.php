<nav class="navbar navbar-expand-lg navbar-light shadow">
  <div class="container d-flex justify-content-between align-items-center">

    <a class="navbar-brand text-warning logo h1 align-self-center" href="<?=BASE_URL;?>">
      <img src="public/img/logo.png" alt="" style="width: 150px;height:130px" />
    </a>

    <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
      <div class="flex-fill">
        <ul class="nav navbar-nav d-flex p-1 mx-lg-auto">
          <li class="nav-item mx-2">
            <a class="nav-link" href="<?= BASE_URL; ?>">Home</a>
          </li>
          <?php if (isset($_SESSION['adminRole'])) : ?>
            <li class="nav-item mx-2">
              <a class="nav-link" href="<?= BASE_URL; ?>dashboard">Admin Panel</a>
            </li>
          <?php endif; ?>
          <li class="nav-item mx-2">
            <a class="nav-link" href="<?= BASE_URL; ?>categories">Catégories</a>
          </li>
          <li class="nav-item mx-2">
            <a class="nav-link" href="<?= BASE_URL; ?>contact">Contact</a>
          </li>

        </ul>
      </div>
      <div class="navbar align-self-center d-flex text-decoration-none">
        <a class="nav-icon position-relative text-decoration-none" href="<?= BASE_URL;?>cart">
          <i class="fa fa-fw fa-cart-arrow-down text-dark mr-1"></i>
          <span class="position-absolute top-0 left-100 translate-middle badge rounded-pill bg-light text-dark">
            <?php
            if (isset($_SESSION['count']) && $_SESSION['count'] > 0) {
              echo $_SESSION['count'];
            } else {
              echo 0;
            }
            ?>
          </span>
        </a>
        <a class="nav-item dropdown text-decoration-none" href="#">
          <a  href="#" id="navbarDropdown" class="text-decoration-none" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-fw fa-user text-dark mr-3"></i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <?php if (!isset($_SESSION['logged'])) : ?>
              <a class="dropdown-item" href="<?= BASE_URL ?>register">Inscription</a>
              <a class="dropdown-item" href="<?= BASE_URL ?>login">Connexion</a>
            <?php else : ?>
              <a class="dropdown-item"><?= $_SESSION['pseudo'] ?></a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="<?= BASE_URL ?>logout">Déconnexion</a>
            <?php endif; ?>
          </div>
          </a>
      </div>
    </div>

  </div>
</nav>















