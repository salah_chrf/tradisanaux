<?php
$data = new CategoriesController();
$ctgs = $data->getAllCategories();


if (isset($_POST['ctgId'])) {
    $data = new ProductsController();
    $products = $data->getProductByPagination($_POST['ctgId']);
} else {
    $data = new ProductsController();
    $products = $data->getProductByPagination(null);
}

?>
<div class="container ">
    <div class="row">

        <div class="col-lg-3">
            <h1 class="h2 pb-4">Categories</h1>
            <ul class="list-unstyled templatemo-accordion">
                <ul id="collapseThree" class="list-unstyled pl-3">
                    <?php foreach ($ctgs as $ctg) : ?>
                        <form id="ctgProducts" method="POST" action="<?= BASE_URL ?>categories">
                            <input type="hidden" name="ctgId" id="inpCtgID" />
                        </form>
                        <li>
                            <a onclick="getCtgProducts(<?= $ctg['idCtg'] ?>)" class="dropdown-item" href="javascript:void(0)">
                                <?= $ctg['titleCtg'] ?>
                            </a>
                        </li>
                    <?php endforeach; ?>

                </ul>
        </div>

        <div class="col-lg-9">
            <div class="row">
            </div>
            <div class="row">
                <?php foreach ($products["data"] as $product) : ?>
                    <div class="col-md-4">
                        <form id="formGetProduct" action="<?= BASE_URL ?>show" method="POST">
                            <input type="hidden" value="" name="productId" />
                        </form>
                        <div class="card mb-4 product-wap rounded-0">
                            <div class="card rounded-0">
                                <img class="card-img rounded-0 img-fluid" src="./public/uploads/<?= $product['imagePrd'] ?>" alt="img">
                                <div class="card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a class="btn btn-success text-white mt-2" onclick="getProduct(<?= $product['idPrd'] ?>)" href="javascript:void(0)">
                                                <i class="far fa-eye"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <form method="POST" action="<?= BASE_URL ?>checkout">
                                                <input type="hidden" name="qnt" value="<?= 1; ?>" />
                                                <input type="hidden" name="productTitle" id="prdTitle" value="<?= $product['titlePrd'] ?>" />
                                                <input type="hidden" name="productId" id="prdId" value="<?= $product['idPrd'] ?>" />
                                                <button type="submit" class="btn btn-success text-white mt-2"><i class="fas fa-cart-plus"></i></button>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <a class="card-title text-decoration-none " onclick="getProduct(<?= $product['idPrd'] ?>)" href="javascript:void(0)">
                                    <?= $product['titlePrd'] ?>
                                </a>
                                <ul class="w-100 list-unstyled d-flex justify-content-between mb-0">
                                    <li>M/L/X/XL</li>
                                    <li class="pt-2">
                                        <span class="product-color-dot color-dot-red float-left rounded-circle ml-1"></span>
                                        <span class="product-color-dot color-dot-blue float-left rounded-circle ml-1"></span>
                                        <span class="product-color-dot color-dot-black float-left rounded-circle ml-1"></span>
                                        <span class="product-color-dot color-dot-light float-left rounded-circle ml-1"></span>
                                        <span class="product-color-dot color-dot-green float-left rounded-circle ml-1"></span>
                                    </li>
                                </ul>
                                <ul class="list-unstyled d-flex justify-content-center mb-1">
                                    <li>
                                        <i class="text-warning fa fa-star"></i>
                                        <i class="text-warning fa fa-star"></i>
                                        <i class="text-warning fa fa-star"></i>
                                        <i class="text-muted fa fa-star"></i>
                                        <i class="text-muted fa fa-star"></i>
                                    </li>
                                </ul>
                                <p class="text-center mb-0"><?= $product['prixPrd'] ?> DH
                                    <s class="text-decoration-line-through text-danger">(<?= $product['OldPrixPrd'] ?> DH)</s>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div div="row">
                    <ul class="pagination pagination-lg justify-content-end">
                        <?php for ($i = 1; $i <= $products["pagesNbr"]; $i++) : ?>
                        <li class="page-item disabled">
                                <form method="POST" action="<?= BASE_URL ?>categories">
                                    <input type="hidden" name="CurPage" value="<?= $i ?>" />
                                    <button class="btn btn-outline-success active rounded-0 mr-3 shadow-sm border-top-0 border-left-0" tabindex="-1" type="submit"><?= $i ?></button>
                                </form>
                                <!-- <a class="page-link active rounded-0 mr-3 shadow-sm border-top-0 border-left-0" href="#" tabindex="-1">1</a> -->
                            </li>
                            <?php endfor; ?>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>