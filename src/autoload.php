<?php
@ob_start();
@session_start();

require_once("bootstrap.php");
spl_autoload_register('autoload');

function autoload($className){
    $arrayPath=array(
        'database/',
        'app/classes/',
        'models/',
        'controllers/'
    );
    $parts=explode('\\',$className);
    $name=array_pop($parts);

    foreach($arrayPath as $path){
        $file=sprintf($path.'%s.php',$name);
        if(is_file($file)){
            include_once $file;
        }
    }
}