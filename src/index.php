<?php

require_once 'autoload.php';

$pages=['home','cart','dashboard',
        'updateProduct','deleteProduct','addProduct',
        'emptyCart','show','cancelCart',
        'register','login','checkout',
        'logout','products','orders','addOrder','orderDetails','categories','Ctgs','addCtg','deleteCtg'
];

$adminRole=['dashboard','updateProduct','deleteProduct','addProduct','orders','products','orderDetails','Ctgs','addCtg','deleteCtg'];

if(isset($_SESSION['adminRole']) && isset($_GET['page']) && in_array($_GET['page'],$adminRole)){
    include_once('views/admin/adminHeader.php');
}else{
    include_once('views/includes/Header.php');
}

$home=new HomeController();

if (isset($_GET['page'])){
    if(in_array($_GET['page'],$pages)){
        $page=$_GET['page'];
        if(in_array($page,$adminRole)){
            if(isset($_SESSION['admin']) && $_SESSION['admin'] == true){
                include_once('views/includes/alerts.php');
                $admin=new AdminController();
                $admin->index($page);
            }else{
                include('views/includes/404.php');
            }
        }else{
            $home->index($page);
        }
    }else{
        include('views/includes/404.php');
    }
}else{
    $home->index('home');
}

if(isset($_SESSION['adminRole']) && isset($_GET['page']) && in_array($_GET['page'],$adminRole)){
    include_once('views/admin/adminFooter.php');
}else{
    include_once('views/includes/footer.php');
}
