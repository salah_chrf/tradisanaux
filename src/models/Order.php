<?php
class Order{
    public static function createOrder($id){
        DB::executerRequete('INSERT into commandes values
        (default,default,:idClient)',[':idClient'=>$id]);
        return 'ok';
    }
    
    public static function getAllOrders(){
        $stm=DB::executerRequete("select lc.idCmd,c.dateAchat,(lc.qte*p.prixPrd) as total,u.nomComplet as nomClient
        from commandes c,users u,lignecommande lc,produits p 
        where c.idClient=u.idUser and c.idCmd=lc.idCmd and lc.idPrd=p.idPrd ;
        ");
        return $stm->fetchAll();
    }
    
    public static function getTotalOrdersPrice(){
        $stm=DB::executerRequete("select sum(lc.qte*p.prixPrd) as total from lignecommande lc,produits p
        where lc.idPrd=p.idPrd");
        return $stm->fetch();
    }
}