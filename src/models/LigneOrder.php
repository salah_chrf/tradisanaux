<?php
class LigneOrder{
    // X
    public static function createLigne($data){
        $stm=DB::executerRequete('select max(idCmd) as numcom from commandes');
        $numcmd=$stm->fetch(PDO::FETCH_OBJ);
        DB::executerRequete('INSERT into lignecommande values
         (default,:idPrd,:idCmd,:qte)',
        [
            ':idPrd'=>$data['idPrd'],
            ':idCmd'=>$numcmd->numcom,
            ':qte'=>$data['qte']
        ]);
        return 'ok';
    }
    
    public static function getAllOrders(){
        $stm=DB::executerRequete("SELECT c.*,u.nomComplet as nomClient,p.titlePrd as produit,p.prixPrd
        from commandes c,produits p,users u where c.idPrd=p.idPrd and c.idClient=u.idUser");
        return $stm->fetchAll();
    }

    public static function getOrderDetails($idcmd){
        $stm=DB::executerRequete("select lc.qte,p.titlePrd,p.prixPrd,p.imagePrd from lignecommande lc,produits p
        where lc.idPrd=p.idPrd and lc.idCmd=:idCommande",[":idCommande"=>$idcmd]);
        return $stm->fetchAll();
    }
}