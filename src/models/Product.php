<?php
class Product
{
    static public function getAll()
    {
        $stm = DB::executerRequete('SELECT * from produits');
        return $stm->fetchAll();
    }


    static public function getProductById($id)
    {
        $stm = DB::executerRequete('SELECT * from produits where idPrd=:id', [':id' => $id]);
        return $stm->fetch(PDO::FETCH_OBJ);
    }

    public static function addProduct($data)
    {
        $stmt = DB::executerRequete('INSERT INTO produits 
        VALUES (default,:titlePrd,:ctgId,:prixPrd,:oldPrixPrd,:qntPrd,
        :descriptionPrd,:miniDscPrd,:imagePrd)', [
            ':titlePrd' => $data['product_title'],
            ':descriptionPrd' => $data['product_description'],
            ':qntPrd' => $data['product_quantity'],
            ':imagePrd' => $data['product_image'],
            ':prixPrd' => $data['product_price'],
            ':oldPrixPrd' => $data['old_price'],
            ':miniDscPrd' => $data['short_desc'],
            ':ctgId' => $data['product_category_id']
        ]);
        return 'ok';
    }
    public static function editProduct($data)
    {
        $stmt = DB::executerRequete(
            'UPDATE  produits set
        titlePrd=:titlePrd,
        ctgId=:ctgId,
        prixPrd=:prixPrd,
        oldPrixPrd=:oldPrixPrd,
        qntPrd=:qntPrd,
        descriptionPrd=:descriptionPrd,
        miniDscPrd=:miniDscPrd,
        imagePrd=:imagePrd where idPrd=:idPrd',
            [
                ':idPrd' => $data['product_id'],
                ':titlePrd' => $data['product_title'],
                ':descriptionPrd' => $data['product_description'],
                ':qntPrd' => $data['product_quantity'],
                ':imagePrd' => $data['product_image'],
                ':prixPrd' => $data['product_price'],
                ':oldPrixPrd' => $data['old_price'],
                ':miniDscPrd' => $data['short_desc'],
                ':ctgId' => $data['product_category_id']
            ]
        );
        return 'ok';
    }

    public static function deleteProduct($data)
    {
        $id = $data['id'];
        try {
            // add more details to delete
            DB::executerRequete('DELETE FROM produits WHERE  idPrd= :idP', [":idP" => $id]);
            return 'ok';
        } catch (PDOException $ex) {
            echo "erreur " . $ex->getMessage();
            die();
        }
    }

    public static function pagination( $categorie = null){
        $nbrProductsPerPage = 3;
        $totalPages = ceil(count(Product::getAll()) / $nbrProductsPerPage);
        if (!isset($_POST['CurPage'])) {
            $_POST['CurPage'] = 1;
        }
        $currentPage = $_POST['CurPage'];
        $offset = ($currentPage - 1) * $nbrProductsPerPage;
        if ($categorie != null) {
            $stm = DB::executerRequete("SELECT * FROM produits where ctgId=:id LIMIT ".$offset.",".$nbrProductsPerPage,
            [':id' => $categorie]);
        }else{
            $stm = DB::executerRequete("SELECT * FROM produits LIMIT ".$offset.",".$nbrProductsPerPage);
        }
        return ['data'=>$stm->fetchAll(),'pagesNbr'=>$totalPages];
    }
    
}
