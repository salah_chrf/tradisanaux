<?php
class Category {
    public static function getAll(){
        $stm=DB::executerRequete('select *  from categories');
        return  $stm->fetchAll();
    }
    public static function AjouterCtg($title){
        $sql="INSERT INTO categories VALUES (default,:title)";
        $stm=DB::executerRequete($sql,[":title"=>$title]);
        if($stm){
            return "ok";
        }else{
            return "error";
        }
    }
    public static function DeleteCtg($id){
        $sql="SELECT * from produits where ctgId=:id";
        $stm=DB::executerRequete($sql,[":id"=>$id]);
        if(count($stm->fetchAll())>0){
            return "error";
        }
        $sql="DELETE from categories where idCtg=:id ";
        $stm=DB::executerRequete($sql,[":id"=>$id]);
        if($stm){
            return "ok";
        }else{
            return false;
        }
    }
}